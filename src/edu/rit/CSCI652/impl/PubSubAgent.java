package edu.rit.CSCI652.impl;

import edu.rit.CSCI652.demo.Event;
import edu.rit.CSCI652.demo.Publisher;
import edu.rit.CSCI652.demo.Subscriber;
import edu.rit.CSCI652.demo.Topic;

import java.io.DataOutputStream;
import java.io.IOException;

import java.net.Socket;
import java.net.InetSocketAddress;

import java.util.ArrayList;

// Sends the information needed for agent actions to occur in the EventManager
public class PubSubAgent implements Publisher, Subscriber{

    private String username;
    private String ipHost;
    private int    port;
    private int    localPort;
    private Socket connection;
    private InetSocketAddress busAddress;
    private DataOutputStream outgoing;

    // Constructor
    public PubSubAgent( String username, String ipHost, int port, int localPort )
    {
        this.username  = username;
        this.ipHost    = ipHost;
        this.port      = port;
        this.localPort = localPort;
        busAddress = new InetSocketAddress( ipHost, port );
    }

    @Override
    // Allows an agent to subscribe to a topic and receive events published under that topic
    public void subscribe(Topic topic)
    {
        // Message format: s TOPIC_ID TOPIC_NAME USERNAME LISTENER_PORT
        // System.out.println( topic.toString() );
        try
            {
                // Startup
                startConnection();
                // Business
                outgoing.writeChar( 's'             ); // subscribe
                outgoing.writeInt(  topic.getID()   ); // Topic's ID
                outgoing.writeUTF(  topic.getName() ); // Topic Name
                outgoing.writeUTF(  username        ); // Agent username
                outgoing.writeInt(  localPort       ); // Listening Port
                outgoing.flush();                      // Send it!
                // Cleanup
                closeConnection();
            }
        catch( Exception e )
            {
                System.err.println( "PubSubAgent - subscribe Exception: " + e );
            }
    }

    @Override
    // IGNORE THIS
    public void subscribe(String keyword)
    {
        // TODO Auto-generated method stub
        // Keywords are outside of the scope of this implementation
    }

    @Override
    // Allows an agent to unsubscribe from a topic and stop receiving events published under that topic
    public void unsubscribe(Topic topic)
    {
        // Message format: u TOPIC_ID TOPIC_NAME USERNAME LISTENER_PORT
        //System.out.println( topic.toString() );
        try
            {
                // Startup
                startConnection();
                // Business
                outgoing.writeChar( 'u' ); // unsubscribe
                outgoing.writeInt( topic.getID()   ); // topic id
                outgoing.writeUTF( topic.getName() ); // topic name
                outgoing.writeUTF( username        ); // agent username
                outgoing.writeInt(  localPort      ); // Listening Port
                outgoing.flush();                     // send it!
                // Cleanup
                closeConnection();
            }
        catch( Exception e )
            {
                System.err.println( "PubSubAgent - unsubscribe Exception: " + e );
            }
    }

    @Override
    // IGNORE THIS
    public void unsubscribe() {
        // TODO Auto-generated method stub
        // DO NOT CALL THIS!
        // DOES NOTHING
    }

    @Override
    // Allows an agent to get a list of topics to which it has subscribed to
    public void listSubscribedTopics()
    {
        // Message format: l USERNAME LISTENER_PORT
        try
            {
                // Startup
                startConnection();
                // Business
                outgoing.writeChar( 'l' );
                outgoing.writeUTF( username );
                outgoing.writeInt( port );
                outgoing.flush();
                // Cleanup
                closeConnection();
            }
        catch( Exception e )
            {
                System.err.println( "PubSubAgent - listSubscribedTopics Exception: " + e );
            }
    }

    @Override
    // Allows an agent to send an event out to other agents that have subscribed to the topic the event was created under
    public void publish(Event event)
    {
        // Message format: p EVENT_ID EVENT_TOPIC_NAME EVENT_TITLE EVENT_CONTENT USERNAME LISTENER_PORT
        // System.out.println( event.toString() );
        try
            {
                // Startup
                startConnection();
                // Business
                outgoing.writeChar( 'p' ); // publish
                outgoing.writeInt( event.getID() );
                outgoing.writeUTF( event.getTopicName() );
                outgoing.writeUTF( event.getTitle() );
                outgoing.writeUTF( event.getContent() );
                outgoing.writeUTF( username );
                outgoing.writeInt( port );
                outgoing.flush();
                // Cleanup
                closeConnection();
            }
        catch( Exception e )
            {
                System.err.println( "PubSubAgent - publish Exception: " + e );
            }
    }

    @Override
    // Allows an agent to inform all other agents that there is a new topic they can subscribe to
    public void advertise(Topic topic)
    {
        // Message Format: a TOPIC_ID TOPIC_NAME USERNAME LISTENER_PORT
        // System.out.println( topic.toString() );
        try
            {
                // Startup
                startConnection();
                // Business
                outgoing.writeChar( 'a'             ); // advertise
                outgoing.writeInt(  topic.getID()   ); // Topic's ID
                outgoing.writeUTF(  topic.getName() ); // Topic Name
                outgoing.writeUTF(  username        ); // Publisher username
                outgoing.writeInt(  port            ); // Publisher's listener port
                outgoing.flush();                      // Send it!
                // Cleanup
                closeConnection();
            }
        catch( Exception e )
            {
                System.err.println( "PubSubAgent - advertise Exception: " + e );
            }
    }

    // Allows an agent to get a list of subscribed users for a topic
    public void getSubscriberList( String topicName )
    {
        // Message format: g TOPIC_NAME USERNAME LISTENER_PORT
        try
            {
                // Startup
                startConnection();
                // Business
                outgoing.writeChar( 'g' );
                outgoing.writeUTF( topicName );
                outgoing.writeUTF( username );
                outgoing.writeInt( port );
                outgoing.flush();
                // Cleanup
                closeConnection();
            }
        catch( Exception e )
            {
                System.err.println( "PubSubAgent - getSubscriberList Exception: " + e );
            }
    }

    // Helper methods

    // Opens a connection with the EventManager and gets the output stream
    private void startConnection()
    {
        try
            {
                connection = new Socket();
                connection.connect( busAddress );
                outgoing = new DataOutputStream( connection.getOutputStream() );
            }
        catch( Exception e )
            {
                System.err.println( "PubSubAgent - startConnection Exception: " + e );
            }
    }

    // Closes the connection with the EventManager
    private void closeConnection()
    {
        try
            {
                connection.close();
            }
        catch( Exception e )
            {
                System.err.println( "PubSubAgent - closeConnection Exception: " + e );
            }
    }
	
}
