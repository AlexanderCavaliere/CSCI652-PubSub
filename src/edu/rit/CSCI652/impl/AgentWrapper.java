package edu.rit.CSCI652.impl;

import edu.rit.CSCI652.demo.Event;
import edu.rit.CSCI652.demo.Publisher;
import edu.rit.CSCI652.demo.Subscriber;
import edu.rit.CSCI652.demo.Topic;

import java.io.DataOutputStream;
import java.io.IOException;

import java.net.Socket;
import java.net.InetSocketAddress;

import java.util.ArrayList;


public class AgentWrapper
{

    public static void main( String[] args )
    {
        // Based on cmd line flags start as either Pub or Sub
        // Have user pass in IP/Port of EventManager
        // May need seperate class to actually create this one
        // Call it UserAgent?

        // ARGS
        // 0: Sub or Pub
        // 1: EventManager IP/Hostname
        // 2: EventManager FrontDesk port num
        // 3: Username

        if( args.length != 5 )
            {
                System.err.println( "Usage: java PubSubAgent pub/sub ip/hostname port username localPort" );
                System.exit(1);
            }

        String pubOrSub   = args[0];
        String ipHost     = args[1];
        int port       = 0;
        int localPort = 0;
        String username   = args[3];

        try
            {
                port = Integer.parseInt( args[2] );
                localPort = Integer.parseInt( args[4] );
            }
        catch( NumberFormatException nfe )
            {
                System.err.println( "Usage: java PubSubAgent pub/sub ip/hostname port username localPort" );
                System.err.println( "port and localPort must be a positive integer");
                System.exit( 1 );
            }
        // Worker threads to handle user input, outgoing messages, and incoming messages
        PubSubSender sender = new PubSubSender( new PubSubAgent( username, ipHost, port, localPort ), pubOrSub );
        PubSubReceiver receiver = new PubSubReceiver( localPort );

        Thread notifier = new Thread( sender );
        Thread listener = new Thread( receiver );

        notifier.start();
        listener.start();
    }
}
