package edu.rit.CSCI652.impl;

import edu.rit.CSCI652.demo.Event;
import edu.rit.CSCI652.demo.Publisher;
import edu.rit.CSCI652.demo.Subscriber;
import edu.rit.CSCI652.demo.Topic;
import edu.rit.CSCI652.impl.PubSubAgent;

import java.util.Scanner;

public class PubSubSender implements Runnable
{
    private final String _PUB_ = "pub";
    private final String _SUB_ = "sub";

    private String pubSubStatus;

    private PubSubAgent agent;

    public PubSubSender( PubSubAgent agent, String pubSubStatus )
    {
        this.agent        = agent;
        this.pubSubStatus = pubSubStatus;
    }

    public void run()
    {
        // Handling user input and invoking PubSub commands via the object
        try
            {
                Scanner scan = new Scanner( System.in );
                scan.useDelimiter( "\n" );
                for( ;; )
                    {
                        System.out.println( "Type help for a list of commands.");
                        System.out.print( ">>> ");
                        char command = scan.next().charAt( 0 );
                        System.out.println();
                        if (pubSubStatus.equalsIgnoreCase( _PUB_ ) )
                            publisherCommands( command, scan );
                        else if( pubSubStatus.equalsIgnoreCase( _SUB_ ) )
                            subscriberCommands( command, scan );
                    }
            }
        catch( Exception e )
            {
                // Don't do anything; just try again
            }
    }

    /*
      Handles command parsing for publisher agents
      Sends the parsed input off to the pubsub agent class for further processing
     */
    private void publisherCommands( char command, Scanner scan )
    {
        String topicName = "";
        String title     = "";
        String content   = "";
        switch( command )
            {
            case 'p': // Publish
                // 'p' Topic_Name Title Content
                System.out.printf( "Enter the name of the topic to publish under: " );
                topicName = scan.next();
                System.out.printf( "\nEnter the title of the article: " );
                title = scan.next();
                System.out.printf( "\nEnter the content below.\n\n" );
                content = scan.next();
                agent.publish( new Event( 0, topicName, title, content ) );
                break;
            case 'n': // New Topic
                // 'n' Topic_Name
                System.out.printf( "Enter the name of the new topic: " );
                topicName = scan.next();
                agent.advertise( new Topic( 0, topicName ) );
                System.out.printf( "\n" );
                break;
            case 'g': // Get List of Subscribers
                // 'g' Topic_Name
                System.out.printf( "Enter the name of the topic: " );
                topicName = scan.next();
                agent.getSubscriberList( topicName );
                System.out.printf( "\n" );
            default: // BAD COMMAND
                printHelp();
                break;
            }
    }

    /*
      Handles command parsing for subscriber agents
      Sends the parsed intput off the pubsub agent class for further processing
     */
    private void subscriberCommands( char command, Scanner scan )
    {
        String topicName = "";
        switch( command )
            {
            case 's': // Subscribe
                // 's' Topic_Name
                System.out.printf( "Enter the name of the topic: " );
                topicName = scan.next();
                System.out.println();
                agent.subscribe( new Topic( 0, topicName ) );
                break;
            case 'u': // Unsubscribe
                System.out.printf( "Enter the name of the topic: " );
                topicName = scan.next();
                System.out.println();
                agent.unsubscribe( new Topic( 0, topicName ) );
                break;
            case 'l': // List Subscribed Topics
                agent.listSubscribedTopics();
                break;
            default: // BAD COMMAND
                printHelp();
                break;
            }
    }

    // Displays the available agent commands to the user
    private void printHelp()
    {
        System.out.printf( "\n##########################################################\n" );
        System.out.printf( "Commands:\n" );
        System.out.printf( "subscribe - Subscribe to a new topic (Sub only)\n" );
        System.out.printf( "unsubscribe - Stop receiving events from a topic (Sub only)\n");
        System.out.printf( "list subscriptions - Display subscriptions (Sub only)\n" );
        System.out.printf( "new - Create a new topic (Pub only)\n" );
        System.out.printf( "publish - Create a new event under a topic (Pub only)\n" );
        System.out.printf( "get subscribers - Display subscribers for a topic (Pub only)\n");
        System.out.printf( "\n##########################################################\n" );
    }
}
