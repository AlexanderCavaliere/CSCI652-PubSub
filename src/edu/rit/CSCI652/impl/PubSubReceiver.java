package edu.rit.CSCI652.impl;

import edu.rit.CSCI652.demo.Event;
import edu.rit.CSCI652.demo.Publisher;
import edu.rit.CSCI652.demo.Subscriber;
import edu.rit.CSCI652.demo.Topic;
import edu.rit.CSCI652.impl.PubSubAgent;

import java.util.Scanner;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetSocketAddress;
import java.net.InetAddress;

import java.io.DataInputStream;

// Handles incoming events from the EventManager and prints them out for the agent to see.
public class PubSubReceiver implements Runnable
{
    private ServerSocket listener;

    private int port;

    public PubSubReceiver( int port )
    {
        this.port = port;
    }

    // Wait for incoming messages from the EventManager
    public void run()
    {
        try
            {
                listener = new ServerSocket();
                listener.bind( new InetSocketAddress( (InetAddress)null, port ) );
                for( ;; )
                    {
                        Socket connection = listener.accept();
                        DataInputStream incoming = new DataInputStream( connection.getInputStream() );

                        char command = incoming.readChar();

                        switch( command )
                            {
                            case 'm': // incoming subscriptions
                                int eventID = incoming.readInt();
                                String eventTitle = incoming.readUTF();
                                String eventContent = incoming.readUTF();

                                System.out.printf( "\n##################\n" );
                                System.out.println( eventTitle );
                                System.out.println( eventContent );
                                System.out.printf( "##################\n" );
                                break;
                            default:
                                break;
                            }
                        // If we have interrupted the agent's console; re-print the prompt
                        System.out.println( "Type help for a list of commands.");
                        System.out.print( ">>> ");
                    }
            }
        catch( Exception e )
            {
                System.err.println( "PubSubReceiver - run Exception: " + e );
            }
    }
}
