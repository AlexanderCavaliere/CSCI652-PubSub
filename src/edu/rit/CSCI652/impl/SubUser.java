package edu.rit.CSCI652.impl;


// Java Lang Classes
import java.net.InetAddress;

// Project
import edu.rit.CSCI652.demo.Subscriber;


public class SubUser
{
    // Class variables
    private InetAddress home;
    private final String username;
    private int port;

    public SubUser( InetAddress home, String username, int port )
    {
        this.home = home;
        this.port = port;
        this.username = username;
    }

    public InetAddress getUserAddress()
    {
        return this.home;
    }

    public String getUsername()
    {
        return this.username;
    }

    public int getPort()
    {
        return this.port;
    }
}

