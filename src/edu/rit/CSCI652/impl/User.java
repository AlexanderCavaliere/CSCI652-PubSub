package edu.rit.CSCI652.impl;


// Java Lang Classes
import java.net.InetAddress;


public class User
{
    // Class variables
    private InetAddress home;
    private final String username;
    private int port;

    public User( InetAddress home, String username, int port )
    {
        this.home = home;
        this.port = port;
        this.username = username;
    }

    public InetAddress getUserAddress()
    {
        return this.home;
    }

    public String getUsername()
    {
        return this.username;
    }

    public int getPort()
    {
        return this.port;
    }
}

