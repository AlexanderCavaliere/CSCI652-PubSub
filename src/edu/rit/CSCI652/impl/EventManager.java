package edu.rit.CSCI652.impl;

// Java Lang Classes
import java.util.HashMap;
import java.util.LinkedList;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.InetAddress;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

// Project
import edu.rit.CSCI652.demo.Event;
import edu.rit.CSCI652.demo.Subscriber;
import edu.rit.CSCI652.demo.Topic;
import edu.rit.CSCI652.demo.EventManagerWorker;
import edu.rit.CSCI652.impl.User;

/*
  Software bus for a toy Pub/Sub system
  You are not considered a member of the system until you have performed an action on the system ( new topic, publish, subscribe )
 */
public class EventManager
{
    private HashMap<String, Topic> existingTopics = new HashMap<String, Topic>();
    private HashMap<Topic,LinkedList<User>> userTopics = new HashMap<Topic,LinkedList<User>>();
    private LinkedList<User> activeUsers = new LinkedList<User>();
    private HashMap<User,LinkedList<Event>> pendingMessages = new HashMap<User,LinkedList<Event>>();
    private static int topicIDCounter = 0;
    private static int eventIDCounter = 0; // This will probably backfire in larger systems but that is outside of scope for this one!
	/*
	 * Start the repo service
	 */
	private void startService( int port )
    {
        // fire up a thread pool
        // one thread to be "front desk" to negotiate incoming connections
        // threads from the pool will be used to handle requests
        ExecutorService executor = Executors.newCachedThreadPool();

        ServerSocket socket = null;
        try
            {
                socket = new ServerSocket();
                socket.bind( new InetSocketAddress( (InetAddress)null, port ) );
            }
        catch( IOException ioe ) {}
        Thread notifier = new Thread( new EventManagerNotifier( this ) );
        notifier.start();
        for( ;; )
            {
                try
                    {
                        // Front desk thread will dispatch workers to carry out requests from clients
                        executor.execute( new EventManagerWorker( this, socket.accept() ) );
                    }
                catch( Exception e )
                    {
                        System.err.println( "EventManager - startService Exception: " + e );
                    }
            }
    }

    /*
      Called by a specific worker thread once a minute (timer starts when EM is started )
      Goes through each user's backlog of messages and tries to send them
      notifyUsers handles removing the message from the backlog
     */
    public synchronized void flushPendingMessages()
    {
        System.out.println( "Flushing Messages" );
        for( User user : pendingMessages.keySet() )
            {
                for( Event message : pendingMessages.get( user ) )
                    notifyUsers( user, message );
            }
    }

    /*
      Takes an incoming event from a publisher agent
      Adds the publisher to the user list
      Logs the event
      Sends the event out to the subscribers associated with the topic the event was published under
     */
    public synchronized void publish( Event event, User publisher )
    {
        boolean found = false;
        for( User existing : activeUsers )
            {
                if( existing.getUsername().equalsIgnoreCase( publisher.getUsername() ) )
                    {
                        existing = publisher;
                        found = true;
                        break;
                    }
            }

        if( !found )
            activeUsers.add( publisher );

        boolean topicFound = false;
        for( String title : existingTopics.keySet() )
            {
                if( title.equalsIgnoreCase( event.getTopicName() ) )
                    {
                        event.setTopic( existingTopics.get( title ) );
                        event.setID( ++eventIDCounter );
                        topicFound = true;
                        break;
                    }
            }

        if( topicFound )
            for( User user : userTopics.get(event.getTopic() ) )
                {
                    notifyUsers( user, event );
                }

    }

    /*
     * notify all subscribers of new event
     * Should be called when a publisher "publishes" new content
     * Handles queue messages up for offline users
     */
    public void notifyUsers( User user, Event event )
    {
        // Get the topic the event falls under
        // Iterate over the list of subs subscribed to that Topic
        // Inform each sub that there is new content
        try
            {
                Socket connection = new Socket();
                connection.connect( new InetSocketAddress( user.getUserAddress(), user.getPort() ) );
                DataOutputStream outgoing = new DataOutputStream( connection.getOutputStream() );

                outgoing.writeChar( 'm' );
                outgoing.writeInt( event.getID() );
                outgoing.writeUTF( event.getTitle() );
                outgoing.writeUTF( event.getContent() );
                outgoing.flush();

                connection.close();

                if( pendingMessages.get( user ).indexOf( event ) != -1 )
                    pendingMessages.get( user ).remove( event );
            }
        catch( Exception e )
            {
                if( pendingMessages.get( user ) != null )
                    {
                        if( pendingMessages.get( user ).indexOf( event ) == -1 )
                            pendingMessages.get( user ).add( event );
                    }
            }
    }
	/*
	 * add new topic when received advertisement of new topic
   # add publisher to user list
   * check to make sure topic doesn't already exist
   * log the topic
   * inform users of new topic
	 */
    public synchronized void addTopic(Topic topic, User publisher )
    {
        boolean found = false;
        for( User existing : activeUsers )
            {
                if( existing.getUsername().equalsIgnoreCase( publisher.getUsername() ) )
                    {
                        existing = publisher;
                        found = true;
                        break;
                    }
            }

        if( !found )
            activeUsers.add( publisher );

        // Keep a list of the existing topic names and their object
        // If a topic with that name already exists we'll skip advertising the topic
        Topic newTopic = null;

        for( String key : existingTopics.keySet() )
            {
                if( key.equalsIgnoreCase( topic.getName() ) )
                    {
                        newTopic = existingTopics.get( key );
                        break;
                    }
            }
        if( newTopic == null )
            {
                topic.setID( ++topicIDCounter );
                existingTopics.put( topic.getName(), topic );
                userTopics.put( topic, new LinkedList<User>() );
                // advertise
                for( User user : activeUsers )
                    {
                        notifyUsers( user, new Event( ++eventIDCounter, topic, "New Topic Available!", topic.getName() ) );
                    }
            }
    }

    /*
      Control method for modifying topic subscriptions
      All the data structures are non-thread safe
      add and remove perfrom write and read operations on the same objects
     */
    public synchronized void modifySubscription( User sub, String topicName, boolean subStatus )
    {
        if( subStatus )
            addSubscriber( sub, topicName );
        else
            removeSubscriber( sub, topicName );
    }

    /*
     * add subscriber to the internal list
     */
    private synchronized void addSubscriber( User sub, String topicName )
    {
        for( String key : existingTopics.keySet() )
            {
                if( key.equalsIgnoreCase( topicName ) )
                    {
                        activeUsers.add( sub );
                        userTopics.get( existingTopics.get( key ) ).add( sub );
                        pendingMessages.put( sub, new LinkedList<Event>() );
                    }
            }
    }

    /*
     * remove subscriber from the list
     */
    private synchronized void removeSubscriber( User sub, String topicName )
    {
        if( existingTopics.get( topicName ) != null)
            {
                for( User user : activeUsers )
                    {
                        if( sub.getUsername().equalsIgnoreCase( user.getUsername() ) )
                            {
                                userTopics.get( existingTopics.get( topicName ) ).remove( user );
                                activeUsers.remove( user );
                                for( Event event : pendingMessages.get( user ) )
                                    {
                                        if( event.getTopicName().equalsIgnoreCase( topicName ) )
                                            pendingMessages.get(user).remove(event);
                                    }
                                System.out.println( activeUsers.size() );
                                break;
                            }
                    }
            }
    }

    /*
     * show the list of subscriber for a specified topic
     */
    public void showSubscribers( String topicName, User requester )
    {
        String userList = "";
        Topic topic = null;
        for( String topics : existingTopics.keySet() )
            if( topicName.equalsIgnoreCase( topics ) )
                {
                    topic = existingTopics.get( topicName );
                    break;
                }

        if( topic != null)
            for( User subscriber : userTopics.get( topic ) )
                {
                    userList += subscriber.getUsername() + " ";
                }

        notifyUsers( requester, new Event( ++eventIDCounter, topic, "Users subscribed to " + topic.getName() + ":\n", userList ) );
    }

    /*
      Informs a subscriber of the topics they are subscribed too
     */
    public void showSubscriptions( User requester )
    {
        String topicList = "";
        boolean active = false;
        for( User user : activeUsers )
            {
                if( user.getUsername().equalsIgnoreCase( requester.getUsername() ) )
                    {
                        active = true;
                        requester = user;
                        break;
                    }
            }
        if( active )
            for( Topic topic : userTopics.keySet() )
                {
                    if( userTopics.get( topic ).contains( requester ) )
                        topicList += topic.getName() + " ";
                }
        notifyUsers( requester, new Event( ++eventIDCounter, "", "You are subscribed to: \n", topicList ) );

    }

    public static void main(String[] args)
    {
        // args[0] will need to be a port number
        if( args.length != 1 )
            {
                System.out.println( "Usage: java EventManager port" );
                System.exit( 1 );
            }
        new EventManager().startService( Integer.parseInt( args[0] ) );
    }


}

// Helper Class
class EventManagerNotifier implements Runnable
{
    private EventManager em;

    public EventManagerNotifier( EventManager em )
    {
        this.em = em;
    }

    public void run()
    {
        for( ;; )
            {
                try
                    {
                        Thread.sleep( 60000 ); // One minute
                    }
                catch( Exception e )
                    {
                        System.err.println( "EventManagerNotifier - run Exception: " + e );
                    }
                em.flushPendingMessages();
            }
    }
}
