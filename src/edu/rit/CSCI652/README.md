Pub/Sub Demo

Assumptions:
1) A user is not registed with the system until it has performed an action on the system (Publish, Subscribe, Create Topic)
2) Usernames are unique and will be associated with one (and only one) ip/port combination
3) All events will be tagged by one topic and only one topic
4) Topic name are case insensitive and must be unique

EventManager

Usage:
java src.edu.rit.CSCI652.impl.EventManager $port_number

The EventManager has a notification thread that tries to send out queued messages for existing users once a minute (timer starts from creation of EM)

AgentWrapper

Usage:
java src.edu.rit.CSCI652.impl.AgentWrapper $role $EM_ip_address $EM_port_number $username $local_port

role = pub or sub (publisher or subscriber)
EM_ip_address = IP of the Event Manager
EM_port_number = Port the Event Manager is listening on
username = Your username
local_port = Port agent wrapper is listening on for messages from Event Manager

Don't hit the enter button at the prompt; you'll break it. Hit ctrl + c to quit.
