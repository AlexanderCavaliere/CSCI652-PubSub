#!/bin/bash

EVENT_MANAGER_PORT=5555
AGENT_PORT=2222
AGENT_PATH="edu/rit/CSCI652/impl/AgentWrapper"
FLAGS="--profile=docker -e"
# Create the docker containers for the demo
docker network create --driver bridge pubsub_network
docker create -it --name EventManager --network pubsub_network --net-alias EventManager arc6393/pubsub 
docker create -it --name AlexSub --network pubsub_network --net-alias AlexSub arc6393/pubsub 
docker create -it --name BlakeSub --network pubsub_network --net-alias BlakeSub arc6393/pubsub 
docker create -it --name CarrieSub --network pubsub_network --net-alias CarrieSub arc6393/pubsub 
docker create -it --name ErinPub --network pubsub_network --net-alias ErinPub arc6393/pubsub 
docker create -it --name DarylPub --network pubsub_network --net-alias DarylPub arc6393/pubsub 
docker create -it --name FreddyPub --network pubsub_network --net-alias FreddyPub arc6393/pubsub 

docker start EventManager
docker start AlexSub
docker start BlakeSub
docker start CarrieSub
docker start ErinPub
docker start DarylPub
docker start FreddyPub

mate-terminal $FLAGS "docker exec -i EventManager java edu/rit/CSCI652/impl/EventManager $EVENT_MANAGER_PORT"
mate-terminal $FLAGS "docker exec -i AlexSub java $AGENT_PATH sub EventManager $EVENT_MANAGER_PORT alex $AGENT_PORT"
mate-terminal $FLAGS "docker exec -i BlakeSub java $AGENT_PATH sub EventManager $EVENT_MANAGER_PORT blake $AGENT_PORT"
mate-terminal $FLAGS "docker exec -i CarrieSub java $AGENT_PATH sub EventManager $EVENT_MANAGER_PORT carrie $AGENT_PORT"
mate-terminal $FLAGS "docker exec -i ErinPub java $AGENT_PATH pub EventManager $EVENT_MANAGER_PORT erin $AGENT_PORT"
mate-terminal $FLAGS "docker exec -i DarylPub java $AGENT_PATH pub EventManager $EVENT_MANAGER_PORT daryl $AGENT_PORT"
mate-terminal $FLAGS "docker exec -i FreddyPub java $AGENT_PATH pub EventManager $EVENT_MANAGER_PORT freddy $AGENT_PORT"
