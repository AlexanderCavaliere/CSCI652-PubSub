package edu.rit.CSCI652.demo;

// Java Lang
import java.net.Socket;
import java.net.InetAddress;
import java.util.concurrent.ThreadLocalRandom;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

// Project
import edu.rit.CSCI652.impl.EventManager;
import edu.rit.CSCI652.demo.Event;
import edu.rit.CSCI652.demo.Subscriber;
import edu.rit.CSCI652.demo.Topic;

import edu.rit.CSCI652.impl.User;

public class EventManagerWorker implements Runnable
{
    // Handles incoming messages from agents for the EventManager
    public void run()
    {
        try
            {
                // Display the incoming connection information (Sender IP, Port, etc )
                // System.out.printf( "###########################\n" );
                // System.out.printf( "Requester IP: %s\n", requesterSocket.getInetAddress().toString() );
                // System.out.printf( "Requester Port: %d\n", requesterSocket.getPort() );
                // System.out.printf( "###########################\n" );

                int eventID = 0;
                int topicID = 0;
                int port    = 0;

                String topicName     = "";
                String eventTitle    = "";
                String eventContent  = "";
                String username      = "";

                DataInputStream incoming = new DataInputStream( requesterSocket.getInputStream() );
                char command = incoming.readChar();

                switch( command )
                    {
                    case 'p': // publish
                        eventID = incoming.readInt();
                        topicName = incoming.readUTF();
                        eventTitle = incoming.readUTF();
                        eventContent = incoming.readUTF();
                        username = incoming.readUTF();
                        port = incoming.readInt();

                        em.publish( new Event( eventID, topicName, eventTitle, eventContent ),
                                    new User( requesterSocket.getInetAddress(), username, port) );
                        // System.out.println( eventID + ", " + topicName + ", " + eventTitle + ", " + eventContent );
                        break;
                    case 'a': // new topic
                        topicID = incoming.readInt();
                        topicName = incoming.readUTF();
                        username = incoming.readUTF();
                        port = incoming.readInt();
                        // System.out.println( topicID + ", " + topicName );
                        // Pass data to the event manager
                        em.addTopic( new Topic( topicID, topicName), new User( requesterSocket.getInetAddress(), username, port) );
                        break;
                    case 's': // subscribe
                        topicID = incoming.readInt();
                        topicName = incoming.readUTF();
                        username = incoming.readUTF();
                        port = incoming.readInt();
                        // System.out.println( topicID + ", " + topicName + ", " + username );

                        em.modifySubscription( new User( requesterSocket.getInetAddress(), username, port ), topicName, true );
                        break;
                    case 'u': // unsubscribe
                        topicID = incoming.readInt();
                        topicName = incoming.readUTF();
                        username = incoming.readUTF();
                        port = incoming.readInt();
                        // System.out.println( topicID + ", " + topicName + ", " + username );

                        em.modifySubscription( new User( requesterSocket.getInetAddress(), username, port ), topicName, false );
                        break;
                    case 'l': // listSubscriptions
                        username = incoming.readUTF();
                        port = incoming.readInt();

                        em.showSubscriptions( new User( requesterSocket.getInetAddress(), username, port) );
                        break;
                    case 'g': // getSubscribers
                        topicName = incoming.readUTF();
                        username = incoming.readUTF();
                        port = incoming.readInt();

                        em.showSubscribers( topicName, new User( requesterSocket.getInetAddress(), username, port) );
                        break;
                    default: // no match
                        break;
                    }
                requesterSocket.close();
            }
        catch( Exception e )
            {
                System.err.println( "EventManagerWorker - run Exception: " + e );
            }
    }

    private EventManager em;
    private Socket requesterSocket;

    public EventManagerWorker( EventManager em, Socket requesterSocket )
    {
        this.em = em;
        this.requesterSocket = requesterSocket;
    }
}
