package edu.rit.CSCI652.demo;

import java.util.List;
import java.util.LinkedList;
import java.io.Serializable;

public class Topic implements Serializable
{
    private int id;
    private List<String> keywords;
    private String name;

    // Constructors
    public Topic( int id, String name )
    {
        this.id = id;
        keywords = new LinkedList<String>();
        this.name = name;
    }

    public Topic( int id, List<String> keywords, String name )
    {
        this.id = id;
        this.keywords = keywords;
        this.name = name;
    }
    // End Constructors

    public List<String> getKeywords()
    {
        return this.keywords;
    }

    public String getName()
    {
        return this.name;
    }

    public int getID()
    {
        return this.id;
    }

    public void addKeyword( String keyword )
    {
        keywords.add( keyword );
    }

    public void setID( int id )
    {
        this.id = id;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public String toString()
    {
        return "TOPIC( " + id + " " + name + " )";
    }
}
