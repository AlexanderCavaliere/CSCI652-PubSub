package edu.rit.CSCI652.demo;

import java.util.List;

public class Event
{
    private int id;
    private Topic topic;
    private String topicName;
    private String title;
    private String content;

    public Event( int id, Topic topic, String title, String content )
    {
        this.id = id;
        this.topic = topic;
        this.title = title;
        this.content = content;
    }

    public Event( int id, String topicName, String title, String content )
    {
        this.id = id;
        this.topicName = topicName;
        this.title = title;
        this.content = content;
    }

    public void setID( int id )
    {
        this.id = id;
    }

    public void setTopic( Topic topic )
    {
        this.topic = topic;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public void setContent( String content )
    {
        this.content = content;
    }


    public void setTopicTitle( String newTitle )
    {
        topicName = newTitle;
    }

    public String getTopicName()
    {
        return this.topicName;
    }

    public int getID()
    {
        return this.id;
    }

    public Topic getTopic()
    {
        return this.topic;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getContent()
    {
        return this.content;
    }

    public String toString()
    {
        return "EVENT( " + id + " " + title + " " + content + " " +  topicName + " )";
    }
}
